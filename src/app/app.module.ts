import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableOneComponent } from './table-one/table-one.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import 'rxjs/add/observable/of';
import { Observable } from 'rxjs';



@NgModule({
  declarations: [
    AppComponent,
    TableOneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng2SmartTableModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
