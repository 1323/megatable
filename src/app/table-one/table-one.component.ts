import { Component, OnInit } from '@angular/core';
// import { Ng2SmartTableModule } from 'ng2-smart-table';

import 'rxjs/add/observable/of';
import { Observable } from 'rxjs';

@Component({
  selector: 'table-one',
  templateUrl: './table-one.component.html',
  styleUrls: ['./table-one.component.scss']
})
export class TableOneComponent implements OnInit {

  settings = {
    columns: {
      id: {
        title: 'ID'
      },
      name: {
        title: 'Full Name'
      },
      username: {
        title: 'User Name'
      },
      email: {
        title: 'Email'
      }
    }
  };

  data = [
    {
      id: 1,
      name: "Leanne Graham",
      username: "Bret",
      email: "Sincere@april.biz"
    },
    {
      id: 2,
      name: "Ervin Howell",
      username: "Antonette",
      email: "Shanna@melissa.tv"
    },
    
    // ... list of items
    
    {
      id: 11,
      name: "Nicholas DuBuque",
      username: "Nicholas.Stanton",
      email: "Rey.Padberg@rosamond.biz"
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
